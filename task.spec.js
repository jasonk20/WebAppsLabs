/*
 * task.spec.js
 *
 * Test file for your task class
 */
var expect, Task;

expect = require('./chai.js').expect;

Task = require('./task.js');

// ADD YOUR TESTS HERE
var t1 = Task.new();
describe('Tests for makeNewTask constructor', function() { 
	it('makeNewTask makes a new task object', function() {
		expect(t1).to.be.a('object');
	});
	it('title equals an empty string', function() {
		expect(t1.title).to.equal('');
	})
	it('title equals an empty string', function() {
		expect(t1.title).to.equal('');
	})
	it('completedTime equals null', function() {
		expect(t1.completedTime).to.equal(null);
	})
	it('tags equals an empty array', function() {
		expect(t1.tags).to.be.a('array');
	})
	it('not able to write over id', function() {
		t1.id = 4;
		expect(t1.id).to.equal(1);
	})
	it('not able to write over tags', function() {
		t1.tags = ['hello', 'hi'];
		expect(t1.tags[0]).to.equal(undefined);
	})
	it('not able to add properties to task object', function() {
		t1.fakeProp = "test";
		expect(t1.fakeProp).to.equal(undefined);
	})
	it('id for the first Task made should equal 0', function() {
		expect(t1.id).to.equal(1);
	})
	it('id for second object should equal 2', function() {
		var t2 = Task.new();
		expect(t2.id).to.equal(2);
	})
});

// var t2 = new Task.prototype.setTitle('hello');
describe('Tests for prototype methods', function() { 
	it('setTitle changes title of task object', function() {
		var t2 = Task.new();
		t2.setTitle('hello');
		expect(t2.title).to.equal('hello');
		expect(t2).to.be.a('object');
	});
	it('setTitle trims whitespace', function() {
		var t3 = Task.new();
		t3.setTitle('    hello      ');
		expect(t3.title).to.equal('hello');
	});
	it('isCompleted should default at false on new object', function() {
		var t4 = Task.new();
		expect(t4.completedTime).to.equal(null);
		expect(t4.isCompleted()).to.equal(false);
	});
	it('toggleCompleted should complete task and set completedTime to current date', function() {
		var t5 = Task.new();
		t5.toggleCompleted();
		expect(t5.isCompleted()).to.equal(true);
		expect(t5.completedTime).to.be.a('Date');
	});
	it('toggleCompleted should set a completed task to null', function() {
		var t6 = Task.new();
		t6.toggleCompleted();
		t6.toggleCompleted();
		expect(t6.isCompleted()).to.equal(false);
	});
	it('hasTag should return false on newly created Task', function() {
		var t7 = Task.new();
		expect(t7.hasTag('hello')).to.equal(false);
	});
	it('hasTag should return true on existing tag', function() {
		var t7 = Task.new();
		t7.addTag('hello')
		expect(t7.hasTag('hello')).to.equal(true);
	});
	it('addTag should add a new tag to the task', function() {
		var t8 = Task.new();
		t8.addTag('hello');
		expect(t8.hasTag('hello')).to.equal(true);
	});
	it('addTag should not add an existing tag to the Task', function() {
		var t9 = Task.new();
		t9.addTag('what');
		expect(function() {t9.addTag('what'); }).to.throw(Error);
	});
	it('removeTag should remove tag from the task', function() {
		var t10 = Task.new();
		t10.addTag('hello');
		t10.addTag('what');
		t10.removeTag('hello');
		expect(t10.hasTag('hello')).to.equal(false);
		expect(t10.hasTag('what')).to.equal(true);
	});
	it('toggleTag should add and remove appropriate tags from the task', function() {
		var t10 = Task.new();
		t10.addTag('hello');
		t10.addTag('what');
		t10.toggleTag('hello');
		expect(t10.hasTag('hello')).to.equal(false);
		expect(t10.hasTag('what')).to.equal(true);
	});
	it('removeTag should not remove a nonexistent tag', function() {
		var t11 = Task.new();
		t11.addTag('what');
		expect(function() {t9.removeTag('hi'); }).to.throw(Error);
	});
	it('removeTags should remove existing tags from array', function() {
		var t12, arr1;
		arr1 = ['what','hello']; 
		t12 =  Task.new();
		t12.addTags(arr1);
		t12.removeTags(arr1);
		expect(t12.hasTag('hello')).to.equal(false);
		expect(t12.hasTag('what')).to.equal(false);
	});
	it('addTags should add new tags from array', function() {
		var t12, arr1;
		arr1 = ['what','hello', 'bird']; 
		t12 =  Task.new();
		t12.addTags(arr1);
		expect(t12.hasTag('hello')).to.equal(true);
		expect(t12.hasTag('what')).to.equal(true);
		expect(t12.hasTag('bird')).to.equal(true);
	});
	it('toggleTags should remove an existing tag', function() {
		var t12, arr1;
		arr1 = ['what']; 
		t12 =  Task.new();
		t12.addTag('hello');
		t12.addTag('what');
		t12.toggleTags(arr1);
		expect(t12.hasTag('hello')).to.equal(true);
		expect(t12.hasTag('what')).to.equal(false);
	});
	it('toggleTags should add a nonexsting tag', function() {
		var t13, arr2;
		arr2 = ['what']; 
		t13 =  Task.new();
		t13.addTag('hello');
		t13.toggleTags(arr2);
		expect(t13.hasTag('hello')).to.equal(true);
		expect(t13.hasTag('what')).to.equal(true);
	});
	it('toggleTags should add and remove propery when given multiple tags in array', function() {
		var t14, arr3;
		arr3 = ['what', 'hi', 'skate', 'bird'];
		t14 = Task.new();
		t14.addTag('skate');
		t14.addTag('what');
		t14.toggleTags(arr3);
		expect(t14.hasTag('what')).to.equal(false);
		expect(t14.hasTag('hi')).to.equal(true);
		expect(t14.hasTag('skate')).to.equal(false);
		expect(t14.hasTag('bird')).to.equal(true);
	});
	it('clone should have same title, completion status and tag list as original Task', function() {
		var t15, clone1;
		t15 = Task.new();
		t15.setTitle('jason');
		t15.addTag('hello');
		t15.addTag('what');
		t15.toggleCompleted();
		clone1 = t15.clone();
		expect(clone1.title).to.equal('jason');
		expect(clone1.completedTime).to.be.a('Date');
		expect(clone1.hasTag('hello')).to.equal(true);
		expect(clone1.hasTag('what')).to.equal(true);
	});
});
describe('Tests for makeTaskFromObject', function() { 
	it('makeTaskFromObject makes a new task object', function() {
		var o1, t16;
		o1 = {
			title: 'jason',
			tags: ['hello', 'yo']
		};
		t16 = Task.fromObject(o1);
		expect(t16).to.be.a('object');
		expect(t16.hasTag('hello')).to.equal(true);
		expect(t16.hasTag('yo')).to.equal(true);
	});
});
describe('Tests for makeTaskFromString', function() { 
	it('makeTaskFromString makes a new task object', function() {
		var str, t1;
		str = '  hey you whats up #howboutdat  ';
		t16 = Task.fromString(str);
		expect(t16).to.be.a('object');
		expect(t16.hasTag('howboutdat')).to.equal(true);
		expect(t16.title).to.equal('hey you whats up');
	});
});

