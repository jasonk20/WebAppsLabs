/*
 * task.js
 *
 * Contains implementation for a "task" "class"
 */

var Task, proto, count;

count = 0;
// Helper method. You should not need to change it.
// Use it in makeTaskFromString
function processString(s) {
   'use strict';
   var tags, title;

   tags = [];
   title = s.replace(/\s*#([a-zA-Z]+)/g, function(m, tag) {
      tags.push(tag);

      return '';
   });

   return { title: title, tags: tags };
}

/*
 *       Constructors
 */
function makeNewTask() {
   var o;

   o = Object.create(proto);

   count += 1;
   Object.defineProperty(o, 'id', {
      value: count,
      enumerable: true,
      configurable: false,
      writable: false
   });
   o.title = '';
   o.completedTime = null;
   Object.defineProperty(o, 'tags', {
      value: [],
      configurable: false,
      writable: false,
      enumerable: false
   });
   Object.preventExtensions(o);

   return o;
}

function makeTaskFromObject(o) {
   var task;

   task = Task.new();
   task.setTitle(o.title);
   task.addTags(o.tags);

   return task;
}

function makeTaskFromString(str) {
   var task;

   task = Task.fromObject(processString(str));

   return task;
}


/*
 *       Prototype / Instance methods
 */

proto = {

    //Add instance methods here
   setTitle: function setTitle(s) {
   		this.title = s.trim();

      return Object(this);
   },
   isCompleted: function isCompleted() {
   		if (this.completedTime === null) {
   			return false;
   		}    		else {
   			return true;
   		}
   },
   toggleCompleted: function toggleCompleted() {
   		if (this.completedTime === null) {
   			this.completedTime = new Date();
   		} else {
   		this.completedTime = null;
   		}

      	return Object(this);
   },
   hasTag: function hasTag(s) {
    	if (this.tags.indexOf(s) === -1) {
    	return false;
    	}	else {
    		return true;
    	}
   },
   addTag: function addTag(s) {
    	if (this.tags.indexOf(s) === -1) {
    		this.tags.push(s);
    	} else {
    		throw new Error('tag already exists');
    	}
   },
   removeTag: function removeTag(s) {
    	if (this.tags.indexOf(s) > -1) {
    		var index;

    		index = this.tags.indexOf(s);
    		delete this.tags[index];
    	} else {
    		throw new Error('tag does not exist');
    	}

      return this;
   },
   toggleTag: function toggleTag(s) {
    	if (this.tags.indexOf(s) === -1) {
    		this.tags.push(s);
    	}	else {
    		var index;

    		index = this.tags.indexOf(s);
    		delete this.tags[index];
    	}

      return this;
   },
   removeTags: function removeTags(a) {
    	var i;

    	for (i = 0; i < a.length; i += 1) {
    		if (this.tags.indexOf(a[i]) > -1) {
    			var index;

    			index = this.tags.indexOf(a[i]);
    			delete this.tags[index];
    		}
    	}

      return this;
   },
   addTags: function addTags(a) {
    	var i;

    	for (i = 0; i < a.length; i += 1) {
    		if (this.tags.indexOf(a[i]) === -1) {
    			this.tags.push(a[i]);
    		}
    	}

      return this;
   },
   toggleTags: function toggleTags(a) {
    	var i;

    	for (i = 0; i < a.length; i += 1) {
    		if (this.tags.indexOf(a[i]) > -1) {
    			var index;

    			index = this.tags.indexOf(a[i]);
    			delete this.tags[index];
    		}    		else {
    			this.tags.push(a[i]);
    		}
    	}

      return this;
   },
   clone: function clone() {
    	var cloneTask;

    	cloneTask = Task.new();
    	cloneTask.setTitle(this.title);
    	cloneTask.completedTime = this.completedTime;
    	cloneTask.toggleTags(this.tags);

      return cloneTask;
   }
};

// DO NOT MODIFY ANYTHING BELOW THIS LINE
Task = {
   new: makeNewTask,
   fromObject: makeTaskFromObject,
   fromString: makeTaskFromString
};

Object.defineProperty(Task, 'prototype', {
   value: proto,
   writable: false
});

module.exports = Task;
